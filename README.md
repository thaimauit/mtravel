# MTravel
A demo app for tale city

## Install modules
Open terminal and cd to source folder and run
```bash
  npm install
```
OR
```bash
  yarn install
```

## Run app
IOS
```bash
  react-native run-ios
```

Android
```bash
  react-native run-android
```

## Username to login
You can choose one from list user below
```bash
  'Eve', 'Thai', 'Jack', '007'
```

## Code to view Itinerary
```bash
  007
```