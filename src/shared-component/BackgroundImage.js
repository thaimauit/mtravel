/**
 * @author: thai.nguyen
 *
 */
import React from "react";
import { Image, StyleSheet, View } from "react-native";
import { BackgroundAssets } from "../assets";

export default class BackgroundImage extends React.Component {
  shouldComponentUpdate() {
    return false;
  }
  render() {
    return (
      <View style={styles.container}>
        <Image source={BackgroundAssets.appBg} style={styles.img} />
        <View style={styles.background} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    position: "absolute",
    top: 0,
    left: 0,
    right: 0,
    bottom: 0
  },
  img: {
    flex: 1,
  },
  background: {
    position: "absolute",
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    backgroundColor: "rgba(0,0,0,0.5)",

  }
});
