/**
 * @author: thai.nguyen 
 * 
 */
const ActivityTypes = {
  PICKUP: 'Pick-up',
  DROPOFF: 'Drop-off',
  CHECKIN: 'Check-in',
  MEAL: 'Meal',
  INDOOR: 'In-door activity',
  OUTDOOR: 'Outdoor activity'
}
const ItineraryValue = {
  code: '007',
  users: ['Eve', 'Thai', 'Jack', '007', ],
  title: 'Mekong Delta - Phu Quoc Tour 5 Days 4 Nights from Saigon',
  description: 'Experience Mekong Delta - Phu Quoc Tour 5 Days 4 Nights is an interesting trip to explore a bee farm, tropical fruits gardens, taste a tasteful cup of authentic honey tea and visit traditional handicraft villages.',
  photo: 'https://ae.visamiddleeast.com/dam/VCOM/global/travel-with-visa/images/visa-travel-carousel-02-1600x900.jpg',
  startDate: '22/06/2019',
  numberOfDays: 5,
  location: 'Mekong Delta, Phu Quoc, Ho Chi Minh',
  activities: [
    {
      day: 1,
      photo: 'https://wanderlust.com/wp-content/uploads/2015/01/travel-yoga-bus.jpg',
      location: 'Mekong Delta',
      description: 'Investigate Mekong Delta',
      activities: [
        {
          startTime: '08.00am',
          duration: 120, // in minute
          title: 'Travel by bus to My Tho',
          description: 'This morning we will start "Mekong Delta - Phu Quoc Tour", travel by bus to My Tho.',
          photo: 'https://wanderlust.com/wp-content/uploads/2015/01/travel-yoga-bus.jpg',
          location: '',
          phoneNumber: '',
          activityType: ActivityTypes.OUTDOOR,
        },
        {
          startTime: '09.30am',
          duration: 120, // in minute
          title: 'Arrive in My Tho',
          description: `Board a wooden motor boat (20 minutes) to Unicorn Island. On this boat along the river’s bank, you’ll be passing through natural creeks, interesting fisherman's port, stilt houses (half on land, half on water).From there, we’ll take you to a bee farm where you have a chance to taste a tasteful cup of authentic honey tea. After that we walk around quiet villages and watch the life of rural people in Mekong Delta.`,
          photo: 'https://www.indochinaexploretours.com/wp-content/uploads/2017/03/exterior_big_4.jpg',
          location: 'Mekong Delta',
          phoneNumber: '',
          activityType: ActivityTypes.OUTDOOR,
        },
        {
          startTime: '12.00am',
          duration: 60, // in minute
          title: 'Lunch time',
          description: `Have a lunch at the restaurant in Mekong Delta`,
          photo: 'https://cdn2.veltra.com/ptr/20180614062639_345726653_13243_0.jpg',
          location: 'Mekong Delta',
          phoneNumber: '',
          activityType: ActivityTypes.INDOOR,
        },
        {
          startTime: '3.00pm',
          duration: 120, // in minute
          title: 'Visit a seasonable orchard ',
          description: `One of the boat’s stops during this trip is at a seasonable orchard that has many different types of tasty tropical fruits for your enjoyments. You can enjoy the fruits for free while checking out the folk music that is typical to only the people in Southern Vietnam`,
          photo: 'http://saigonodysseyhotel.com/userfiles/image/mk10.jpg',
          location: 'Mekong Delta',
          phoneNumber: '',
          activityType: ActivityTypes.OUTDOOR,
        },
        {
          startTime: '3.00pm',
          duration: 120, // in minute
          title: 'Check-in hotel',
          description: `Check-in hotel and stay overnight in Can Tho.`,
          photo: 'https://t-ec.bstatic.com/images/hotel/max1024x768/130/130915364.jpg',
          location: 'Cần thơ',
          phoneNumber: '',
          activityType: ActivityTypes.CHECKIN,
        },
      ]
    },
    {
      day: 2,
      photo: 'https://samanthalepoet.files.wordpress.com/2018/02/cai_rang.jpg',
      location: 'Mekong Delta',
      description: 'Investigate Cai Rang floating market',
      activities: [
        {
          startTime: '7.15am',
          duration: 120, // in minute
          title: 'Visit Cai Rang floating market',
          description: `After breakfast, we board a boat and travel for about 40 minutes to get to the area's spectacular landscape ----  the biggest Cai Rang floating market...
          This is the largest of its type in the whole Mekong delta. During this visit, you'll have a chance to board a merchant boat that sells pineapples which come from Can Tho City.`,
          photo: 'https://samanthalepoet.files.wordpress.com/2018/02/cai_rang.jpg',
          location: 'Cai Rang floating market',
          phoneNumber: '',
          activityType: ActivityTypes.OUTDOOR,
        },
        {
          startTime: '09.00pm',
          duration: 120, // in minute
          title: 'Arrive Phu Quoc',
          description: `You arrive in Duong Dong airport in Phu Quoc, car/van picks you up from airport and transfer to hotel/resort. Overnight in Phu Quoc.`,
          photo: 'https://q-ec.bstatic.com/images/hotel/max1024x768/127/127245129.jpg',
          location: 'Duong Dong airport',
          phoneNumber: '',
          activityType: ActivityTypes.CHECKIN,
        }
      ]
    }, 
    {
      day: 3,
      photo: 'https://upload.wikimedia.org/wikipedia/commons/6/68/Breakfast_in_spice_restaurant_%282940558133%29.jpg',
      location: 'Phu Quoc',
      description: 'Travel arround Phu Quoc',
      activities: [
        {
          startTime: '08.00am',
          duration: 120, // in minute
          title: 'Breakfast at hotel/resort and relax',
          description: `Breakfast at hotel/resort.  After that, you are free until our guide take you to restaurant for lunch.`,
          photo: 'https://upload.wikimedia.org/wikipedia/commons/6/68/Breakfast_in_spice_restaurant_%282940558133%29.jpg',
          location: 'Phu Quoc',
          phoneNumber: '',
          activityType: ActivityTypes.INDOOR,
        },
        {
          startTime: '02.00pm',
          duration: 300, // in minute
          title: 'Travel arround Phu Quoc',
          description: `Visiting the biggest fishing port and village in Phu Quoc. Then you will trek Tranh stream (from May to Nov only), Hung Long Tu Pagoda and traditional fish sauce processing house`,
          photo: 'https://static1.squarespace.com/static/52ccee75e4b00bc0dba03f46/t/5b188498352f53e06ab0a64b/1528333760010/Premier+Village+Phu+Quoc+-+island+%282%29.jpg',
          location: 'Phu Quoc',
          phoneNumber: '',
          activityType: ActivityTypes.OUTDOOR,
        },

      ]
    },
    {
      day: 4,
      photo: 'https://phuquocxanh.com/vi/wp-content/uploads/2016/12/th%E1%BB%A3-t%C3%A1ch-ng%E1%BB%8Dc-trai.jpg',
      location: 'Phu Quoc',
      description: 'Travel arround Phu Quoc',
      activities: [
        {
          startTime: '07.30am',
          duration: 60, // in minute
          title: 'Breakfast at hotel/resort',
          description: `After breakfast, Free complimentary Breakfast at the hotel/resort’s restaurant.`,
          photo: 'https://upload.wikimedia.org/wikipedia/commons/6/68/Breakfast_in_spice_restaurant_%282940558133%29.jpg',
          location: 'Phu Quoc',
          phoneNumber: '',
          activityType: ActivityTypes.INDOOR,
        },
        {
          startTime: '08.30am',
          duration: 300, // in minute
          title: 'Go arround Phu Quoc',
          description: `Checking out some secrets of a pearl farm. Enjoy the busy activities of an ocean-front seafood marketplace and the fantastic scene of hundreds of colorful fishing boats cluttering the area.`,
          photo: 'https://phuquocxanh.com/vi/wp-content/uploads/2016/12/th%E1%BB%A3-t%C3%A1ch-ng%E1%BB%8Dc-trai.jpg',
          location: 'Phu Quoc',
          phoneNumber: '',
          activityType: ActivityTypes.OUTDOOR,
        },
        {
          startTime: '06.00pm',
          duration: 300, // in minute
          title: 'Shopping at night market',
          description: 'After dinner time, free at leisure, shopping at night market',
          photo: 'http://halongbayairport.com/wp-content/uploads/2017/09/Night-market-in-Phu-Quoc-is-one-of-amazing-local-activities.jpg',
          location: 'Phu Quoc',
          phoneNumber: '',
          activityType: ActivityTypes.OUTDOOR,
        }
      ]
    },
    {
      day: 5,
      photo: 'https://global-goose.com/wp-content/uploads/2013/05/backpacking.jpg',
      location: 'Ho Chi Minh',
      description: 'Come back to Ho Chi Minh',
      activities: [
        {
          startTime: '08.00am',
          duration: 300, // in minute
          title: 'Finish Mekong Delta - Phu Quoc 5 Days 4 Nights Tour.',
          description: 'Breakfast at hotel/resort. Free until transfer to airport for boarding to Ho Chi Minh city. End of services in Phu Quoc. Finish Mekong Delta - Phu Quoc 5 Days 4 Nights Tour.',
          photo: 'https://global-goose.com/wp-content/uploads/2013/05/backpacking.jpg',
          location: 'Phu Quoc',
          phoneNumber: '',
          activityType: ActivityTypes.OUTDOOR,
        }
      ]
    }
  ]
}

export {
  ItineraryValue,
  ActivityTypes
}