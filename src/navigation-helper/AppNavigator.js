/**
 * @author: thai.nguyen
 *
 */
import { Animated, Easing } from "react-native";
import { createStackNavigator, createAppContainer } from "react-navigation";

import ScreenIDs from "./ScreenIDs";
import { Login, EnterCodeScreen, ItineraryDetail, ItineraryDayDetail} from "../screens";

const MainStack = createStackNavigator(
  {
    [ScreenIDs.EnterCodeScreen]: {
      screen: EnterCodeScreen,
      navigationOptions: {
        header: null,
      }
    },
    [ScreenIDs.ItineraryDetail]: {
      screen: ItineraryDetail,
      navigationOptions: {
        header: null,
      }
    },
    [ScreenIDs.ItineraryDayDetail]: ({
      screen: ItineraryDayDetail,
      navigationOptions: {
        headerStyle: {
          backgroundColor: "#1879ff"
        },
        headerTitleStyle: {
          color: "#fff"
        },
        headerTintColor: '#fff'
      }
    })
  },
  {
    headerMode: "float",
    navigationOptions: {
      headerStyle: {
        backgroundColor: "#1879ff"
      },
      headerTitleStyle: {
        color: "#fff"
      }
    },
    transitionConfig: () => ({
      transitionSpec: {
        duration: 300,
        easing: Easing.out(Easing.poly(4)),
        timing: Animated.timing
      },
      screenInterpolator: sceneProps => {
        const { layout, position, scene } = sceneProps;
        const { index } = scene;

        const height = layout.initHeight;
        const translateY = position.interpolate({
          inputRange: [index - 1, index, index + 1],
          outputRange: [height, 0, 0]
        });

        const opacity = position.interpolate({
          inputRange: [index - 1, index - 0.99, index],
          outputRange: [0, 1, 1]
        });

        return { opacity, transform: [{ translateY }] };
      }
    })
  }
);

const LoginStack = createStackNavigator(
  {
    [ScreenIDs.Login]: {
      screen: Login
    }
  },
  {
    headerMode: "none"
  }
);

const AppNavigator = createAppContainer(MainStack);
const AuthNavigator = createAppContainer(LoginStack);

export { AppNavigator, AuthNavigator };
