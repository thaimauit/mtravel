/**
 * @author: thai.nguyen
 *
 */
import {AppNavigator, AuthNavigator} from "./AppNavigator";
import ScreenIDs from './ScreenIDs';

export { AppNavigator, AuthNavigator, ScreenIDs};
