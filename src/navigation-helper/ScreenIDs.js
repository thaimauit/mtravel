/**
 * @author: thai.nguyen 
 *  
 * Define Screen IDs
 */

export default {
  Login: 'screen.Login',
  EnterCodeScreen: 'screen.EnterCodeScreen',
  ItineraryDetail: 'screen.ItineraryDetail',
  ItineraryDayDetail: 'screen.ItineraryDayDetail'
}
