/**
 * @author: thai.nguyen
 *
 */
import React from "react";
import { StyleSheet, ActivityIndicator, View } from "react-native";
import {connect, Provider} from 'react-redux';

import { createReduxContainer, store, rehydrateStore} from "./redux";
import { AuthNavigator, AppNavigator } from "./navigation-helper";

const MainContainer = connect((state) => ({
  state: state.nav,
}))(createReduxContainer(AppNavigator));

class AppContainer extends React.Component {
  render(){
    console.log("logged",this.props.logged);
    if(this.props.logged){
      return <MainContainer />
    }

    return <AuthNavigator />
  }
}

const AppReduxContainer = connect((state)=>({logged: state.profile.username}))(AppContainer)

export default class App extends React.Component {
  state = {
    showApp: false
  };
  componentDidMount() {
    this.initApp();
  }

  async initApp() {
    await rehydrateStore(store);
    this.setState({ showApp: true });
  }

  render() {
    const { showApp } = this.state;
    if (!showApp)
      return (
        <View style={styles.loadingView}>
          <ActivityIndicator size="large" />
        </View>
      );
    return (
      <Provider store={store}>
        <AppReduxContainer />
      </Provider>
    );
  }
}

const styles = StyleSheet.create({
  loadingView: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center"
  }
});
