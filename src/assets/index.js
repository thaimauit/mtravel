/**
 * @author: thai.nguyen
 * 
 */
const BackgroundAssets = {
  appBg: require('./background/bg.jpg')
}

const OtherAssets = {
  thumbnail: require('./others/thumbnail.png')
}

const ItineraryAssets = {
  description: require('./itinerary/description.png'),
  location: require('./itinerary/location.png'),
  startDate: require('./itinerary/startDate.png'),
  users: require('./itinerary/users.png')
}

const CommonAssets = {
  arrowLeft: require('./common/arrow-left.png'),
  arrowRight: require('./common/arrow-right.png'),
}

export {
  BackgroundAssets,
  OtherAssets,
  ItineraryAssets,
  CommonAssets
}
