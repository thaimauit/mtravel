/**
 * @author: thai.nguyen 
 * 
 */
import { persistStore, } from 'redux-persist';

export default function rehydrateStore(store) {
  if (!store) throw new Error('Can\'t start the redux without input store');

  return new Promise((resolve, reject) => {
    try {
      persistStore(store, null, ()=>{
        // console.warn('Store rehydrated', { at: 'rehydrateStore', });
        resolve();
      });
    } catch (e) {
      Logger.error(e, { at: 'rehydrateStore', }, 'error');
      reject(e);
    }
  });
}
