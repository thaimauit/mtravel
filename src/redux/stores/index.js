/**
 * @author: thai.nguyen
 *
 */
import { combineReducers } from "redux";
import { createNavigationReducer } from "react-navigation-redux-helpers";
import {
  actions as profileActions,
  Types as profileTypes,
  reducer as profileReducer
} from "./profile";

import { AppNavigator } from "../../navigation-helper";

const actions = {
  profile: profileActions
};

const types = {
  profile: profileTypes
};

const rootReducer = combineReducers({
  profile: profileReducer,
  nav: createNavigationReducer(AppNavigator)
});

export { actions, types, rootReducer };
