/**
 * @author: thai.nguyen 
 * 
 */
import {Types} from './actions';

const initState = {
  username: '',
  currentTab: 0
}

export default function profileReducer(state = initState, action){
  switch(action.type){
    case Types.UPDATE_USERNAME:
      return {
        ...state,
        username: action.payload
      }
    case Types.UPDATE_TAB:
      return {
        ...state,
        currentTab: action.payload
      }
    default:
      return state;
  }
}