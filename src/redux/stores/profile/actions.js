/**
 * @author: thai.nguyen 
 * 
 */

const Types = {
  LOGIN: 'profile.LOGIN',
  UPDATE_USERNAME: 'profile.UPDATE_USERNAME',
  SUBMIT_CODE: 'profile.SUBMIT_CODE',
  UPDATE_TAB: 'profile.UPDATE_TAB'
}

const actions = {
  login: (username) => ({
    type: Types.LOGIN,
    payload: username
  }),
  updateUsername: (username) => ({
    type: Types.UPDATE_USERNAME,
    payload: username
  }),
  submitCode: (code) => ({
    type: Types.SUBMIT_CODE,
    payload: code
  }),
  updateTab: (tab) => ({
    type: Types.UPDATE_TAB,
    payload: tab
  })
}

export {
  Types,
  actions
}
