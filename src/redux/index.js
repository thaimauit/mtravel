/**
 * @author: thai.nguyen
 *
 */
import { createStore, compose, applyMiddleware} from "redux";

import { persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";

import {
  createReduxContainer,
  createReactNavigationReduxMiddleware,
} from 'react-navigation-redux-helpers';

import createSagaMiddleware from 'redux-saga';

import rootSaga from './sagas';

import { rootReducer } from "./stores";
import rehydrateStore from "./rehydrateStore";

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const navigationMiddleware = createReactNavigationReduxMiddleware(
  state => state.nav,
);

const sagaMiddleware = createSagaMiddleware();

const middleWares = [navigationMiddleware, sagaMiddleware]

const enhancer = composeEnhancers(applyMiddleware(...middleWares));

const persistConfig = {
  key: "root",
  storage,
  whitelist: ["profile"]
};

const persistedReducer = persistReducer(persistConfig, rootReducer);

// Create store (not rehydrate yet)
const store = createStore(persistedReducer, enhancer);

sagaMiddleware.run(rootSaga);

export { store, rehydrateStore, createReduxContainer};
