/**
 * @author: thai.nguyen 
 * 
 */
import {takeLatest} from 'redux-saga/effects';

import { types } from '../../stores';
import loginSaga from './loginSaga';
import submitCodeSaga from './submitCodeSaga';

function * watchLogin(){
  yield takeLatest(types.profile.LOGIN, loginSaga)
}

function *watchSubmitCode(){
  yield takeLatest(types.profile.SUBMIT_CODE, submitCodeSaga)
}

export default [
  watchLogin(),
  watchSubmitCode()
]
