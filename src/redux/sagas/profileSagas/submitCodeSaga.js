/**
 * @author: thai.nguyen
 *
 */

import { put } from "redux-saga/effects";
import { Alert } from "react-native";
import { NavigationActions, StackActions } from "react-navigation";

import { ItineraryValue } from "../../../values";

import { ScreenIDs } from "../../../navigation-helper";

const INVALID_CODE = "Invalid code!";

// list User:
export default function* submitCodeSaga(action) {
  try {
    const code = action.payload;
    if (code !== ItineraryValue.code) throw new Error(INVALID_CODE);
    yield put(
      NavigationActions.navigate({ routeName: ScreenIDs.ItineraryDetail })
    );
  } catch ({ message = "Unknow Error" }) {
    Alert.alert("Sorry!", message);
  }
}
