/**
 * @author: thai.nguyen
 *
 */
import { call, put } from "redux-saga/effects";
import { Alert } from "react-native";
import { ItineraryValue } from "../../../values";
import { actions } from "../../stores/profile";

const USER_NOT_FOUND = "User not found!";

// list User:
export default function* loginSaga(action) {
  try {
    const username = action.payload;
    const user = ItineraryValue.users.find(userF => (userF == username));
    if (!user) throw new Error(USER_NOT_FOUND);
    yield put(actions.updateUsername(username));
  } catch ({ message = "Unknow Error" }) {
    Alert.alert("Sorry!", message);
  }
}
