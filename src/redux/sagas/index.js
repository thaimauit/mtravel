/**
 * @author: thai.nguyen 
 * 
 */
import {all} from 'redux-saga/effects';

import profileSagas from './profileSagas';

export default function*() {
  yield all([
    ...profileSagas
  ])
}