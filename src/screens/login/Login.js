/**
 * @author: thai.nguyen \
 *
 */
import React from "react";
import {
  StyleSheet,
  View,
  Text,
  TextInput,
  TouchableOpacity,
  KeyboardAvoidingView,
  Platform
} from "react-native";

import { BackgroundImage } from "../../shared-component";

export default class Login extends React.Component {
  state = { name: "" };

  _changeUsername = name => this.setState({ name });

  render() {
    const { name } = this.state;
    const { login } = this.props;

    const ViewContainer = Platform.select({
      ios: KeyboardAvoidingView,
      android: View
    });
    const propsOS = Platform.select({
      ios: { behavior: 'padding' },
      android: {}
    })

    return (
      <ViewContainer style={styles.container} {...propsOS}>
        <BackgroundImage />
        <View style={styles.appNameView}>
          <Text style={styles.appName}>M-TRAVEL</Text>
        </View>
        <TextInput
          style={styles.input}
          value={name}
          onChangeText={this._changeUsername}
          underlineColorAndroid="transparent"
          placeholder="Enter your username"
          placeholderTextColor="rgba(255,255,255,0.5)"
          onSubmitEditing={login(name)}
        />
        <View style={styles.btnView}>
          <TouchableOpacity style={styles.btnLogin} onPress={login(name)}>
            <Text style={styles.textLogin}>Login</Text>
          </TouchableOpacity>
        </View>
      </ViewContainer>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    padding: '8%'
  },
  appNameView: {
    position: "absolute",
    top: 100,
    left: 0,
    right: 0,
    alignItems: "center"
  },
  appName: {
    fontSize: 20,
    fontWeight: "700",
    color: "#fff"
  },
  input: {
    borderBottomWidth: 1,
    borderColor: "#fff",
    margin: 15,
    padding: 5,
    textAlign: "center",
    color: "#fff",
    fontSize: 16
  },
  btnView: {
    margin: 10,
    marginTop: 0
  },
  btnLogin: {
    paddingHorizontal: 20,
    paddingVertical: 10,
    borderRadius: 5,
    backgroundColor: "#4286f4",
    alignItems: "center"
  },
  textLogin: {
    color: "#fff",
    fontWeight: "600",
    fontSize: 16
  }
});
