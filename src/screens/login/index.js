/**
 * @author: thai.nguyen
 *
 */
import { connect } from "react-redux";

import Login from "./Login";
import { actions } from "../../redux/stores";

const mapDispatchToProps = dispatch => ({
  login: (username) => () => dispatch(actions.profile.login(username))
});

export default connect(
  state => ({ username: state.profile.username }),
  mapDispatchToProps
)(Login);
