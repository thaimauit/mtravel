/**
 * @author: thai.nguyen 
 *
 * 
 */
import {connect} from 'react-redux';

import ItineraryDetail from './ItineraryDetail';
import { actions } from '../../redux/stores';


const mapStateToProps = null;

const mapDispatchToProps = (dispatch, {navigation}) => ({
  init: () => dispatch(actions.profile.updateTab(0)),
  close: () => navigation.goBack()
})

export default connect(mapStateToProps, mapDispatchToProps)(ItineraryDetail);
