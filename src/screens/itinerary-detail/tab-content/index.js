/**
 * @author: thai.nguyen 
 * 
 */
import {connect} from 'react-redux';

import TabContent from './TabContent';

const mapStateToProps = (state) => ({
  currentTab: state.profile.currentTab? state.profile.currentTab: 0
});

export default connect(mapStateToProps)(TabContent);