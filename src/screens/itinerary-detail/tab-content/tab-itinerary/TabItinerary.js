import React from "react";
import { StyleSheet, View, TouchableOpacity, Text, Dimensions} from "react-native";
import Carousel from "react-native-snap-carousel";

import { ItineraryValue } from "../../../../values";
import ItineraryDay from "./ItineraryDay";

const {width} = Dimensions.get("window");

export default class TabItinerary extends React.Component {
  _renderItem = ({item, index}) => {
    return <ItineraryDay item={item} index={index} onPressItem={this.props.onPressItem(item, index)}/>
  }
  render() {
    return (
      <View style={styles.container}>
        <Carousel
          ref={c => {
            this._carousel = c;
          }}
          data={ItineraryValue.activities}
          renderItem={this._renderItem}
          sliderWidth={width}
          itemWidth={0.8 * width}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});
