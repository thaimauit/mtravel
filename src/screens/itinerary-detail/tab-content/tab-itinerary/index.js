/**
 * @author: thai.nguyen
 *
 */
import { connect } from "react-redux";
import TabItinerary from "./TabItinerary";
import { NavigationActions } from "react-navigation";
import { ScreenIDs } from "../../../../navigation-helper";
const mapDispatchToProps = dispatch => ({
  onPressItem: (item, index) => () =>
    dispatch(
      NavigationActions.navigate({
        routeName: ScreenIDs.ItineraryDayDetail,
        params: { item, index }
      })
    )
});
export default connect(null, mapDispatchToProps)(TabItinerary);
