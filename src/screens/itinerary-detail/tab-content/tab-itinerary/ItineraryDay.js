import React from "react";
import { StyleSheet, Image, TouchableOpacity, View, Text } from "react-native";

import { OtherAssets, ItineraryAssets } from "../../../../assets";

export default class ItineraryDay extends React.Component {
  render() {
    const { item, onPressItem } = this.props;
    const { photo } = item;
    return (
      <TouchableOpacity style={styles.container} onPress={onPressItem}>
        <Image
          style={styles.imgStyle}
          source={{ uri: photo }}
          defaultSource={OtherAssets.thumbnail}
        />
        <View style={styles.infoView}>
          <Text style={styles.desc}>
            <Text style={styles.dayName}>Day {item.day}: </Text>
            {item.description}
          </Text>
          <View style={styles.locationView}>
            <Image style={styles.icon} source={ItineraryAssets.location} />
            <Text style={styles.textLocation}>{item.location}</Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    height: "100%",
    borderWidth: 1,
    borderColor: "rgba(0,0,0,0.1)",
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10
  },
  imgStyle: {
    flex: 1,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10
  },
  infoView: {
    position: "absolute",
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: "rgba(0,0,0,0.5)",
    padding: 10
  },
  dayName: {
    color: "#fff",
    fontSize: 20,
    fontWeight: "700"
  },
  locationView: {
    flexDirection: "row",
    alignItems: "center",
    marginTop: 5
  },
  icon: {
    width: 20,
    height: 20,
    tintColor: "#fff"
  },
  textLocation: {
    color: "#fff",
    marginLeft: 5
  },
  desc: {
    color: "#fff",
    fontSize: 16
  }
});
