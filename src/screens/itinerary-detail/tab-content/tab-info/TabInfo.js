/**
 * @author: thai.nguyen 
 * 
 */
import React from "react";
import { StyleSheet, View, Text, Image } from "react-native";

import { ItineraryAssets } from "../../../../assets";
import { ItineraryValue } from "../../../../values";

const fields = [
  {
    key: "location",
    label: "Location"
  },
  { key: "startDate", label: "From Date" },
  { key: "users", label: "Users" },
  { key: "description", label: "Description" }
];

export default class TabInfo extends React.Component {
  _renderField = field => {
    let content = ItineraryValue[field.key];
    if (field.key === "users") {
      content = "";
      ItineraryValue.users.map(user => (content += user + ", "));
    }
    return (
      <View style={styles.fieldContainer} key={field.key}>
        <View style={styles.labelView}>
          <Image source={ItineraryAssets[field.key]} style={styles.iconField} />
          <Text style={styles.labelText}>{field.label}</Text>
        </View>
        <Text style={styles.textContent}>{content}</Text>
      </View>
    );
  };

  render() {
    return (
      <View style={styles.container}>{fields.map(this._renderField)}</View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    padding: 15
  },
  fieldContainer: {
    marginBottom: 15,
    paddingBottom: 10,
    borderBottomWidth: 1,
    borderColor: "rgba(0,0,0,0.1)"
  },
  labelView: {
    flexDirection: "row",
    alignItems: "center"
  },
  iconField: {
    width: 20,
    height: 20,
    tintColor: "rgba(0,0,0,0.8)"
  },
  labelText: {
    color: "rgba(0,0,0,0.6)",
    marginLeft: 10
  },
  textContent: {
    color: "rgba(0,0,0,0.9)",
    marginTop: 5,
    marginLeft: 20
  }
});
