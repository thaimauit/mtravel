import React from "react";
import { StyleSheet, View, TouchableOpacity, Text } from "react-native";
import TabInfo from "./tab-info";
import TabItinerary from "./tab-itinerary";

export default class TabContent extends React.Component {
  shouldComponentUpdate({currentTab}){
    return currentTab !== this.props.currentTab
  }
  render(){
    const {currentTab} = this.props;
    if(currentTab === 0){
      return <TabInfo />
    }
    return <TabItinerary />
  }
}