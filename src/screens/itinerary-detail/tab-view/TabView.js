/**
 * @author: thai.nguyen
 *
 */

import React from "react";
import { StyleSheet, View, TouchableOpacity, Text } from "react-native";

const tabItems = [
  {
    title: "Info"
  },
  {
    title: "Itinerary"
  }
];

export default class TabView extends React.Component {
  shouldComponentUpdate({ currentTab }) {
    return currentTab !== this.props.currentTab;
  }

  _renderTabItem = (tab, index) => {
    const { currentTab, changeTab } = this.props;
    const tabSide = index == 0 ? styles.tabLeft : styles.tabRight;
    const selectedStyle = currentTab === index ? styles.selectedTab : {};
    const selectedTextStyle =
      currentTab === index ? styles.selectedTabText : {};
    return (
      <TouchableOpacity
        style={[tabSide, styles.tabItem, selectedStyle]}
        onPress={changeTab(index)}
        activeOpacity={1}
        key={tab.title}
      >
        <Text style={[styles.tabText, selectedTextStyle]}>{tab.title}</Text>
      </TouchableOpacity>
    );
  };

  render() {
    return (
      <View style={styles.container}>{tabItems.map(this._renderTabItem)}</View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    justifyContent: "center",
    marginVertical: 10
  },
  tabItem: {
    width: 80,
    alignItems: "center",
    paddingVertical: 8,
    backgroundColor: "#3e84e8"
  },
  tabLeft: {
    borderTopLeftRadius: 5,
    borderBottomLeftRadius: 5,
    borderRightWidth: 1,
    borderColor: "rgba(0,0,0,0.5)"
  },
  tabRight: {
    borderTopRightRadius: 5,
    borderBottomRightRadius: 5
  },
  selectedTab: {
    backgroundColor: "#9b9fa5"
  },
  tabText: {
    color: "#fff",
    fontWeight: "700"
  }
});
