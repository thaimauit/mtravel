/**
 * @author: thai.nguyen 
 * 
 */
import {connect} from 'react-redux';

import TabView from './TabView';
import { actions } from '../../../redux/stores/profile';

const mapStateToProps = (state) => ({
  currentTab: state.profile.currentTab? state.profile.currentTab: 0
});
const mapDispatchToProps = (dispatch) => ({
  changeTab: (tab) => () => {
    dispatch(actions.updateTab(tab))
  }
})

export default connect(mapStateToProps, mapDispatchToProps)(TabView);