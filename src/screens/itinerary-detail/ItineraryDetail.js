/**
 * @author: thai.nguyen
 *
 */
import React from "react";
import {
  StyleSheet,
  View,
  Text,
  ImageBackground,
  TouchableOpacity,
  Image
} from "react-native";

import { ItineraryValue } from "../../values";
import { OtherAssets, CommonAssets } from "../../assets";
import TabView from "./tab-view";
import TabContent from "./tab-content";

export default class ItineraryDetail extends React.Component {
  shouldComponentUpdate() {
    return false;
  }

  componentDidMount() {
    this.props.init();
  }

  render() {
    return (
      <View style={styles.container}>
        <ImageBackground
          source={{ uri: ItineraryValue.photo }}
          style={styles.coverPhoto}
          defaultSource={OtherAssets.thumbnail}
        >
          <View style={styles.titleView}>
            <Text style={styles.title} numberOfLines={2}>
              {ItineraryValue.title}
            </Text>
          </View>
        </ImageBackground>

        <TabView />

        <TabContent />
        
        <TouchableOpacity style={styles.btnClose} onPress={this.props.close}>
          <Image style={styles.iconClose} source={CommonAssets.arrowLeft} />
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff"
  },
  coverPhoto: {
    height: 200,
    width: "100%",
    justifyContent: "flex-end"
  },
  titleView: {
    backgroundColor: "rgba(0,0,0,0.3)",
    paddingHorizontal: 10,
    paddingVertical: 5
  },
  title: {
    color: "#fff",
    fontWeight: "700",
    fontSize: 18
  },
  desc: {
    color: "rgba(0,0,0,0.8)",
    padding: 10
  },
  btnClose: {
    width: 40,
    height: 40,
    borderRadius: 20,
    alignItems: "center",
    justifyContent: "center",
    position: "absolute",
    top: 20,
    left: 10,
    backgroundColor: "#1879ff"
  },
  iconClose: {
    width: 20,
    height: 20,
    tintColor: "#fff"
  }
});
