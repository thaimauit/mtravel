/**
 * @author: thai.nguyen
 *
 */
import React from "react";
import { StyleSheet, View, Text, Image } from "react-native";

import { OtherAssets } from "../../assets";

const fields = ["title", "description"];

export default class ActivityItem extends React.Component {
  _renderField = (field) => {
    const {item} = this.props;
    return <View style={styles.fieldView} key={field}>
      <View style={styles.fieldPoint} />
      <Text style={styles.contentField}>{item[field]}</Text>
    </View>
  }
  render() {
    const { item } = this.props;
    const { startTime, photo } = item;
    return (
      <View style={styles.container}>
        <View style={styles.startPoint} />
        <View style={styles.contentView}>
          <Text style={styles.startTime}>{startTime}</Text>
          {fields.map(this._renderField)}
          <Image
            style={styles.img}
            source={{ uri: photo }}
            defaultSource={OtherAssets.thumbnail}
          />
          
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    marginBottom: 15
  },
  contentView: {
    flex: 1
  },
  startPoint: {
    width: 10,
    height: 10,
    borderRadius: 5,
    borderWidth: 3,
    borderColor: "green",
    backgroundColor: "#fff",
    marginRight: 10,
    marginTop: 5
  },
  img: {
    width: "100%",
    height: 200,
    marginTop: 10
  },
  fieldView: {
    flexDirection: 'row',

  },
  fieldPoint: {
    width: 10,
    height: 10,
    borderRadius: 5,
    borderWidth: 3,
    borderColor: "rgba(0,0,0,0.6)",
    backgroundColor: "#fff",
    marginRight: 10,
    marginTop: 5
  },
  startTime: {
    color: 'rgba(0,0,0,0.8)',
    fontSize: 16
  }
});
