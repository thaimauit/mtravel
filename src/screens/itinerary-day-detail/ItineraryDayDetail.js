/**
 * @author: thai.nguyen
 *
 */

import React from "react";
import {
  StyleSheet,
  View,
  ActivityIndicator,
  FlatList,
  TouchableOpacity,
  Image
} from "react-native";

import ActivityItem from "./ActivityItem";
import { ItineraryValue } from "../../values";
import { CommonAssets } from "../../assets";

export default class ItineraryDayDetail extends React.Component {
  static navigationOptions = ({ navigation }) => {
    return {
      title: "Day " + navigation.state.params.item.day
    };
  };

  state = {
    loading: false
  };

  componentDidMount() {
    this.setState({
      ...this.props.navigation.state.params
    });
  }

  _key = (item, key) => key + "";

  _renderItem = ({ item }) => <ActivityItem item={item} />;

  _prevousDay = () => {
    const { index } = this.state;
    if (index == 0) return;
    this.setState({ loading: true });

    const newIndex = index - 1;
    const newItem = ItineraryValue.activities[newIndex];

    // set new title
    this.props.navigation.setParams({ item: newItem });

    setTimeout(() => {
      this.setState({
        item: newItem,
        index: newIndex,
        loading: false
      });
    }, 2000);
  };

  _nextDay = () => {
    const { index } = this.state;
    if (index == ItineraryValue.numberOfDays - 1) return;

    this.setState({ loading: true });
    const newIndex = index + 1;
    const newItem = ItineraryValue.activities[newIndex];

    // set new title
    this.props.navigation.setParams({ item: newItem });

    setTimeout(() => {
      this.setState({
        item: newItem,
        index: newIndex,
        loading: false
      });
    }, 2000);
  };

  render() {
    const { item, loading, index } = this.state;
    if (!item) return <View />;
    const { activities } = item;

    const disableLeftBtn = loading == true ? true : index == 0;
    const styleLeftBtn = disableLeftBtn ? styles.disableBtn : {};

    const disableRightBtn =
      loading == true ? true : index == ItineraryValue.numberOfDays - 1;
    const styleRightBtn = disableRightBtn ? styles.disableBtn : {};

    return (
      <View style={styles.container}>
        {!loading && (
          <FlatList
            data={activities}
            keyExtractor={this._key}
            renderItem={this._renderItem}
          />
        )}
        {ItineraryValue.numberOfDays > 1 && (
          <View style={styles.pagingView}>
            <TouchableOpacity
              style={[styles.btn]}
              disabled={disableLeftBtn}
              onPress={this._prevousDay}
            >
              <Image
                source={CommonAssets.arrowLeft}
                style={[styles.icon, styleLeftBtn]}
              />
            </TouchableOpacity>
            <TouchableOpacity
              style={[styles.btn, styleRightBtn]}
              disabled={disableRightBtn}
              onPress={this._nextDay}
            >
              <Image source={CommonAssets.arrowRight} style={styles.icon} />
            </TouchableOpacity>
          </View>
        )}
        {loading && (
          <View style={styles.loadingView}>
            <ActivityIndicator size="large" />
          </View>
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 15
  },
  loadingView: {
    position: "absolute",
    top: 0,
    left: 0,
    right: 0,
    paddingVertical: 5,
    alignItems: "center"
  },
  pagingView: {
    position: "absolute",
    bottom: 15,
    left: 15,
    flexDirection: "row"
  },
  btn: {
    backgroundColor: "#1879ff",
    padding: 10,
    marginHorizontal: 5
  },
  icon: {
    width: 20,
    height: 20,
    tintColor: "#fff"
  },
  disableBtn: {
    opacity: 0.3
  }
});
