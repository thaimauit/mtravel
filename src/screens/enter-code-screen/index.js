/**
 * @author: thai.nguyen
 *
 */
import { connect } from "react-redux";

import EnterCodeScreen from "./EnterCodeScreen";
import { actions } from "../../redux/stores";

const mapStateToProps = null;
const mapDispatchToProps = (dispatch) => {
  const submitCode = (code) => () => {
    dispatch(actions.profile.submitCode(code))
  }

  return {
    submitCode
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(
  EnterCodeScreen
);
