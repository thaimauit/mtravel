/**
 * @author: thai.nguyen \
 *
 */

import React from "react";
import {
  Text,
  View,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  Platform,
  KeyboardAvoidingView
} from "react-native";

import { BackgroundImage } from "../../shared-component";

export default class EnterCodeScreen extends React.Component {
  state = {
    code: ""
  };

  _changeYourCode = code => this.setState({ code });

  render() {
    const { code } = this.state;
    const { submitCode } = this.props;

    const ViewContainer = Platform.select({
      ios: KeyboardAvoidingView,
      android: View
    });
    const propsOS = Platform.select({
      ios: { behavior: 'padding' },
      android: {}
    })
    return (
      <ViewContainer style={styles.container} {...propsOS}>
        <BackgroundImage />
        <View style={styles.appNameView}>
          <Text style={styles.appName}>M-TRAVEL</Text>
        </View>
        <TextInput
          style={styles.input}
          value={code}
          onChangeText={this._changeYourCode}
          placeholderTextColor="rgba(255,255,255,0.5)"
          underlineColorAndroid="transparent"
          placeholder="Enter your code"
          onSubmitEditing={submitCode(code)}
        />
        <View style={styles.btnView}>
          <TouchableOpacity style={styles.btnLogin} onPress={submitCode(code)}>
            <Text style={styles.textLogin}>Enter</Text>
          </TouchableOpacity>
        </View>
      </ViewContainer>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    padding: '8%'
  },
  appNameView: {
    position: "absolute",
    top: 100,
    left: 0,
    right: 0,
    alignItems: "center"
  },
  appName: {
    fontSize: 20,
    fontWeight: "700",
    color: "#fff"
  },
  input: {
    borderBottomWidth: 1,
    borderColor: "#fff",
    margin: 15,
    padding: 5,
    textAlign: "center",
    color: "#fff",
    fontSize: 16
  },
  btnView: {
    margin: 10,
    marginTop: 10
  },
  btnLogin: {
    paddingHorizontal: 20,
    paddingVertical: 10,
    borderRadius: 5,
    backgroundColor: "#4286f4",
    alignItems: "center"
  },
  textLogin: {
    color: "#fff",
    fontWeight: "600",
    fontSize: 16
  }
});
